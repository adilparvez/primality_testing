package catam.numbertheory.primality;

import static catam.numbertheory.primality.BigIntegerUtils.*;

import java.math.BigInteger;

public class PrimalityUtils {
	
	public static boolean trialDivision(BigInteger num) {
		if (num.compareTo(TWO) < 0) {
			return false;
		}
		BigInteger limit = intSqrt(num);
		for (BigInteger i = TWO; i.compareTo(limit) <= 0; i = i.add(ONE)) {
			if (num.mod(i).equals(ZERO)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean trialDivision(long num) {
		return trialDivision(BigInteger.valueOf(num));
	}

	public static boolean fermat(BigInteger num, BigInteger base) {
		if (base.compareTo(ONE) <= 0 || base.compareTo(num) >= 0) { 
			throw new IllegalArgumentException();
		}
		return modPow(base, num.subtract(ONE), num).equals(ONE);
	}
	
	public static boolean fermat(long num, long base) {
		return fermat(BigInteger.valueOf(num), BigInteger.valueOf(base));
	}
	
	public static boolean euler(BigInteger num, BigInteger base) {
		if (num.mod(TWO).equals(ZERO) || base.compareTo(ONE) <= 0 || base.compareTo(num) >= 0) { 
			throw new IllegalArgumentException();
		}
		return modPow(base, num.subtract(ONE).divide(TWO), num).equals(BigInteger.valueOf(jacobi(base, num)).mod(num));
	}
	
	public static boolean euler(long num, long base) {
		return euler(BigInteger.valueOf(num), BigInteger.valueOf(base));
	}

	public static boolean strong(BigInteger num, BigInteger base) {
		int r = num.subtract(ONE).getLowestSetBit();
		BigInteger s = num.subtract(ONE).shiftRight(r);
		BigInteger power = modPow(base, s, num);
		if (power.equals(ONE)) {
			return true;
		}
		for (int i = 0; i < r - 1; i++) {
			if (power.equals(NEGATIVE_ONE.mod(num))) {
				return true;
			}
			power = power.multiply(power).mod(num);
		}
		if (power.equals(NEGATIVE_ONE.mod(num))) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean strong(long num, long base) {
		return strong(BigInteger.valueOf(num), BigInteger.valueOf(base));
	}
	
	public static boolean weakAKS(long num, int r) {
		BigInteger bigNum = BigInteger.valueOf(num);
		// 1
		Polynomial one = new Polynomial(ONE, 0);
		// X^r - 1
		Polynomial polynomialModulus = new Polynomial(ONE, r).subtract(one);
		// (X + 1)^N mod (N, X^r + 1)
		Polynomial lhs = new Polynomial(ONE, 1).add(one).modPow(bigNum, bigNum, polynomialModulus);
		// X^N + 1 mod (N, X^r + 1)
		Polynomial rhs = new Polynomial(ONE, 1).modPow(bigNum, bigNum, polynomialModulus).add(one);

		if (lhs.equals(rhs)) {
			return true;
		}
		return false;
	}
	
	private static int oldLimit = 0;
	public static boolean[] isPrime;
	
	public static void sieve(int limit) {
		if (limit > oldLimit) {
			oldLimit = limit;
			isPrime = new boolean[limit + 1];
	        for (int i = 2; i <= limit; i++) {
	            isPrime[i] = true;
	        }
	        for (int i = 2; i*i <= limit; i++) {
	            if (isPrime[i]) {
	                for (int j = i; i*j <= limit; j++) {
	                    isPrime[i*j] = false;
	                }
	            }
	        }
		}
	}
	
	public static int getNumberOfPrimesInInterval(int min, int max) {
		sieve(max);
		int count = 0;
		for (int i = Math.max(2, min); i <=max; i++) {
			if (isPrime[i]) {
				count++;
			}
		}
		return count;
	}
	
}
