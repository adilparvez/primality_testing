package catam.numbertheory.primality;

import java.math.BigInteger;

public class BigIntegerUtils {

	public static final BigInteger NEGATIVE_ONE = BigInteger.valueOf(-1L);
	public static final BigInteger ZERO = BigInteger.ZERO;
	public static final BigInteger ONE = BigInteger.ONE;
	public static final BigInteger TWO = BigInteger.valueOf(2L);
	public static final BigInteger THREE = BigInteger.valueOf(3L);
	public static final BigInteger FOUR = BigInteger.valueOf(4L);
	public static final BigInteger SEVEN = BigInteger.valueOf(7L);
	public static final BigInteger EIGHT = BigInteger.valueOf(8L);

	public static BigInteger intSqrt(BigInteger num) {
		BigInteger ans = num.divide(TWO);
		while (ans.compareTo(num.divide(ans)) > 0) {
			ans = num.divide(ans).add(ans).divide(TWO);
		}
		return ans;
	}

	public static BigInteger modPow(BigInteger base, BigInteger exponent, BigInteger modulus) {
		BigInteger ans = ONE;
		while (exponent.compareTo(ZERO) > 0) {
			if (exponent.mod(TWO).equals(ONE)) {
				ans = ans.multiply(base).mod(modulus);
			}
			exponent = exponent.divide(TWO);
			base = base.multiply(base).mod(modulus);
		}
		return ans;
	}
	
	public static int jacobi(BigInteger a, BigInteger N) {
		if (N.compareTo(ZERO) <= 0 || N.mod(TWO).equals(ZERO)) {
			throw new IllegalArgumentException();
		}
	
		if (a.equals(ZERO)) {
			if (N.equals(ONE)) {
				return 1;
			} else {
				return 0;
			}
		} else if (a.equals(TWO)) {
			BigInteger temp = N.mod(EIGHT);
			if (temp.equals(ONE) || temp.equals(SEVEN)) {
				return 1;
			} else {
				return -1;
			}
		} else if (a.compareTo(N) >= 0) {
			return jacobi(a.mod(N), N);
		} else if (a.mod(TWO).equals(ZERO)) {
			return jacobi(TWO, N) * jacobi(a.divide(TWO),N);
		} else {
			if (a.mod(FOUR).equals(THREE) && N.mod(FOUR).equals(THREE)) {
				return - jacobi(N, a);
			} else {
				return jacobi(N, a);
			}
		}
	}
	
}
