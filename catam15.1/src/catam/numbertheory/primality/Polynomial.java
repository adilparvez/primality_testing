package catam.numbertheory.primality;

import java.math.BigInteger;
import java.util.Arrays;

import static catam.numbertheory.primality.BigIntegerUtils.*;

public class Polynomial {

	private BigInteger [] coefficients;
	private int degree;
	
	private static void clear(Polynomial poly) {
		for (int i = 0; i < poly.coefficients.length; i++) {
			poly.coefficients[i] = ZERO;
		}
		poly.degree = 0;
	}
	
	private static void fixDegree(Polynomial poly) {
		poly.degree = poly.coefficients.length - 1;
		while (poly.coefficients[poly.degree].compareTo(ZERO) == 0 && poly.degree > 0) {
			poly.degree--;
		}
	}
	
	public Polynomial(int length) {
		coefficients = new BigInteger[length + 1];
		clear(this);
	}
	
	public Polynomial(Polynomial other) {
		coefficients = Arrays.copyOf(other.coefficients, other.coefficients.length);
		degree = other.degree;
	}
	
	public Polynomial(BigInteger coefficient, int power) {
		this(power);
		if (coefficient.compareTo(ZERO) != 0) {
			coefficients[power] = coefficient;
			degree = power;
		}
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Polynomial)) {
			return false;
		}
		Polynomial poly = (Polynomial) other;
		if (degree != poly.degree) {
			return false;
		}
		for (int i = 0; i <= degree; i++) {
			if (coefficients[i].compareTo(poly.coefficients[i]) != 0) {
				return false;
			}
		}
		return true;
	}

	public Polynomial add(Polynomial other) {
		int max = Math.max(degree, other.degree);
		Polynomial ans = new Polynomial(max);
		ans.degree = degree;
		for (int i = 0; i <= max; i++) {
			if (i <= degree && i <= other.degree) {
				ans.coefficients[i] = coefficients[i].add(other.coefficients[i]);
			} else if (i > degree) {
				ans.coefficients[i] = other.coefficients[i];
			} else {
				ans.coefficients[i] = coefficients[i];
			}
		}
		//fixDegree(ans);
		return ans;
	}
	
	public Polynomial subtract(Polynomial other) {
		int max = Math.max(degree, other.degree);
		Polynomial ans = new Polynomial(max);
		ans.degree = degree;
		for (int i = 0; i <= max; i++) {
			if (i <= degree && i <= other.degree) {
				ans.coefficients[i] = coefficients[i].subtract(other.coefficients[i]);
			} else if (i > degree) {
				ans.coefficients[i] = other.coefficients[i].negate();
			} else {
				ans.coefficients[i] = coefficients[i];
			}
		}
		fixDegree(ans);
		return ans;
	}

	public Polynomial multiply(Polynomial other) {
		Polynomial ans = new Polynomial(degree + other.degree);
		ans.degree = degree + other.degree;
		for (int i = 0; i <= degree; i++) {
			for (int j = 0; j <= other.degree; j++) {
				ans.coefficients[i + j] = ans.coefficients[i + j].add(coefficients[i].multiply(other.coefficients[j]));
			}
		}
		//fixDegree(ans);
		return ans;
	}
	
	public Polynomial mod(BigInteger modulus) {
		Polynomial ans = new Polynomial(this);
		for (int i = 0; i <= degree; i++) {
			ans.coefficients[i] = ans.coefficients[i].mod(modulus);
		}
		fixDegree(ans);
		return ans;
	}

	
	public Polynomial mod(Polynomial modulus) {
		Polynomial ans = new Polynomial(this);
		while (ans.degree >= modulus.degree) {
			Polynomial temp = modulus.multiply(new Polynomial(ans.coefficients[ans.degree], ans.degree - modulus.degree));
			ans = ans.subtract(temp);
			//fixDegree(ans);
		}
		return ans;
	}
	
	public Polynomial modPow(BigInteger exponent, BigInteger numberModulus, Polynomial polynomialModulus) {
		int numBits = exponent.bitLength();
		Polynomial ans = new Polynomial(ONE, 0);
		for (int i = 0; i < numBits; i++) {
			ans = ans.multiply(ans).mod(polynomialModulus).mod(numberModulus);
			if (exponent.testBit(numBits - 1 - i)) {
				ans = ans.multiply(this).mod(polynomialModulus).mod(numberModulus);
			}
		}
		return ans;
	}
	
}
