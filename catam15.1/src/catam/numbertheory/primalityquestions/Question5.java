package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;
import java.math.BigInteger;

import catam.numbertheory.primality.PrimalityUtils;

public class Question5 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		strongPseudoprimes(1000000, 2);
		absoluteStrongPseudoprimes(1000000);
	}
	
	public static void strongPseudoprimes(long max, int base) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question5-strong-base=" + base + "-[0," + max + "].txt", "UTF-8");
			pw.println("Strong pseudoprimes base " + base + " in range [" + 0 + "," + max + "]");
			for (long i = 3; i <= max; i +=2) {
				if (PrimalityUtils.strong(i, base) && !PrimalityUtils.trialDivision(i)) {
					pw.println(i);
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static long gcd(long a, long b) {
		return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
	}
	
	public static void absoluteStrongPseudoprimes(int max) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question5-absolute-strong-[0," + max + "].txt", "UTF-8");
			pw.println("Absolute strong pseudoprimes in range [" + 0 + "," + max + "]");
			PrimalityUtils.sieve(max);
			for (int i = 3; i <= max; i += 2) {
				if (!PrimalityUtils.isPrime[i]) {
					int baseCount = 1;
					boolean isAbsolute = true;
					for (int a = 2; a < i; a++) {
						if (gcd(a, i) == 1) {
							if (!PrimalityUtils.strong(i, a)) {
								isAbsolute = false;
								break;
							}
							baseCount++;
						}
					}
					if (isAbsolute) {
						pw.println(i + " is an absolute strong pseudoprime.");
					} else if (baseCount > 1) {
						pw.println(i + " composite in " + baseCount + " bases.");
					}
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}