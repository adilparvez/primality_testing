package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;
import java.util.ArrayList;

import catam.numbertheory.primality.PrimalityUtils;

public class Question6 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		
		PrimalityUtils.sieve((int) (Math.pow(10, 9) + Math.pow(10, 5)));
		for (int k = 5; k <= 9; k++) {
			System.out.println(k);
			int min = (int) Math.pow(10, k);
			int max = min + (int) Math.pow(10, 5);
			int numberOfPrimes = PrimalityUtils.getNumberOfPrimesInInterval(min, max);
			System.out.println("fermat");
			ArrayList<Integer> fermatBase2 = fermatPseudoprimes(min, max, 2);
			ArrayList<Integer> fermatBase3 = fermatPseudoprimes(min, max, 3);
			System.out.println("euler");
			ArrayList<Integer> eulerBase2 = eulerPseudoprimes(min, max, 2);
			ArrayList<Integer> eulerBase3 = eulerPseudoprimes(min, max, 3);
			System.out.println("strong");
			ArrayList<Integer> strongBase2 = strongPseudoprimes(min, max, 2);
			ArrayList<Integer> strongBase3 = strongPseudoprimes(min, max, 3);
			printToFile(k, numberOfPrimes, fermatBase2, "fermat", "2", min, max);
			printToFile(k, numberOfPrimes, fermatBase3, "fermat", "3", min, max);
			printToFile(k, numberOfPrimes, intersect(fermatBase2, fermatBase3), "fermat", "2&3", min, max);
			printToFile(k, numberOfPrimes, eulerBase2, "euler", "2", min, max);
			printToFile(k, numberOfPrimes, eulerBase3, "euler", "3", min, max);
			printToFile(k, numberOfPrimes, intersect(eulerBase2, eulerBase3), "euler", "2&3", min, max);
			printToFile(k, numberOfPrimes, strongBase2, "strong", "2", min, max);
			printToFile(k, numberOfPrimes, strongBase3, "strong", "3", min, max);
			printToFile(k, numberOfPrimes, intersect(strongBase2, strongBase3), "strong", "2&3", min, max);
		}
	}
	
	private static ArrayList<Integer> intersect(ArrayList<Integer> list1, ArrayList<Integer> list2) {
		ArrayList<Integer> intersection = new ArrayList<Integer>();
		for (Integer i : list1) {
			if (list2.contains(i)) {
				intersection.add(i);
			}
		}
		return intersection;
	}
	
	private static void printToFile(int k, int numberOfPrimes, ArrayList<Integer> pseudoPrimes, String type, String bases, long min, long max) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question6-k=" + k + "-" + type + "-base=" + bases + "-[" + min + "," + max + "].txt", "UTF-8");
			pw.println("Number of primes in interval: " + numberOfPrimes);
			pw.println("Number of pseudoprimes: " + pseudoPrimes.size());
			for (Integer i : pseudoPrimes) {
				pw.println(i);
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static ArrayList<Integer> fermatPseudoprimes(int min, int max, int base) {
		ArrayList<Integer> fermatPseudoprimes = new ArrayList<Integer>();
		for (int i = Math.max(3, min); i <= max; i++) {
			if (PrimalityUtils.fermat(i, base) && !PrimalityUtils.isPrime[i]) {
				fermatPseudoprimes.add(i);
			}
		}
		return fermatPseudoprimes;
	}

	public static ArrayList<Integer> eulerPseudoprimes(int min, int max, int base) {
		ArrayList<Integer> eulerPseudoprimes = new ArrayList<Integer>();
		for (int i = Math.max(3, min); i <= max; i ++) {
			if ((i % 2 == 1) && PrimalityUtils.euler(i, base) && !PrimalityUtils.isPrime[i]) {
				eulerPseudoprimes.add(i);
			}
		}
		return eulerPseudoprimes;
	}
	
	public static ArrayList<Integer> strongPseudoprimes(int min, int max, int base) {
		ArrayList<Integer> strongPseudoprimes = new ArrayList<Integer>();
		for (int i = Math.max(3, min); i <= max; i ++) {
			if ((i % 2 == 1) && PrimalityUtils.strong(i, base) && !PrimalityUtils.isPrime[i]) {
				strongPseudoprimes.add(i);
			}
		}
		return strongPseudoprimes;
	}
	
	
}
