package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;
import java.math.BigInteger;

import catam.numbertheory.primality.PrimalityUtils;

public class Question3 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		fermatPseudoprimes(1000000, 2);
		carmichaelNumbers(1000000);
	}
	
	private static void fermatPseudoprimes(long max, int base) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question3-fermat-base=" + base + "-[0," + max + "].txt", "UTF-8");
			pw.println("Fermat pseudoprimes base " + base + " in range [" + 0 + "," + max + "]");
			for (long i = 3; i <= max; i++) {
				if (PrimalityUtils.fermat(i, base) && !PrimalityUtils.trialDivision(i)) {
					pw.println(i);
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static long gcd(long a, long b) {
		return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
	}
	
	private static void carmichaelNumbers(int max) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question3-absolute-fermat-[0," + max + "].txt", "UTF-8");
			pw.println("Absolute fermat pseudoprimes in range [" + 0 + "," + max + "]");
			PrimalityUtils.sieve(max);
			for (int i = 3; i <= max; i++) {
				if (!PrimalityUtils.isPrime[i]) {
					int baseCount = 1;
					boolean isCarmichael = true;
					for (int a = 2; a < i; a++) {
						if (gcd(a, i) == 1) {
							if (!PrimalityUtils.fermat(i, a)) {
								isCarmichael = false;
								break;
							}
							baseCount++;
						}
					}
					if (isCarmichael) {
						pw.println(i + " is carmichael.");
					} else if (baseCount > 1) {
						pw.println(i + " composite in " + baseCount + " bases.");
					}
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
