package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;

import catam.numbertheory.primality.PrimalityUtils;

public class Question2 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		fermatPseudoprimes(188000, 188200, 2, 13);
		fermatPseudoprimes((long) Math.pow(10, 9), (long) Math.pow(10, 9) + 200, 2, 13);
	}
	
	private static void fermatPseudoprimes(long low, long high, int aMin, int aMax) {
		try {
			PrintWriter pw;
			for (int a = aMin; a <= aMax; a++) {
				pw = new PrintWriter(directory + "question2-fermat-base=" + a + "-[" + low + "," + high + "].txt", "UTF-8");
				pw.println("Fermat pseudoprimes base " + a + " in range [" + low + "," + high + "]");
				for (long i = low; i <= high; i++) {
					if (PrimalityUtils.fermat(i, a) && !PrimalityUtils.trialDivision(i)) {
						pw.println(i);
					}
				}
				pw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
