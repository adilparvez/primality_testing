package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;
import catam.numbertheory.primality.PrimalityUtils;

public class Question7 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		
		PrimalityUtils.sieve(1000000);
		for (int r = 1; r <= 6; r++) {
			System.out.println("r = " + r);
			weakAKSPseudoprimes(1000000, r);
		}
	}
	
	private static void weakAKSPseudoprimes(int max, int r) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question7-weakAKS-r=" + r + "-[0," + max + "].txt", "UTF-8");
			pw.println("Weak AKS pseudoprimes mod (X^" + r  + " - 1)" + " in range [" + 0 + "," + max + "]");
			int count = 0;
			for (int i = 2; i <= max; i++) {
				if (PrimalityUtils.weakAKS(i, r) && !PrimalityUtils.isPrime[i]) {
					pw.println(i);
				}
				if (count % 1000 == 0) {
					count = 0;
					System.out.println("i = " + i);
				}
				count++;
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
