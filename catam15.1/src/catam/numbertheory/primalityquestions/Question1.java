package catam.numbertheory.primalityquestions;

import java.io.PrintWriter;

import catam.numbertheory.primality.PrimalityUtils;

public class Question1 {
	
	private static String directory;
	
	public static void main(String[] args) {
		directory = args[0];
		primesInRange(188000, 188200);
		primesInRange((long) Math.pow(10, 9), (long) Math.pow(10, 9) + 200);
	}
	
	private static void primesInRange(long low, long high) {
		try {
			PrintWriter pw = new PrintWriter(directory + "question1-trial-[" + low + "," + high + "].txt", "UTF-8");
			pw.println("Primes in range [" + low + "," + high + "]");
			for (long i = low; i <= high; i++) {
				if (PrimalityUtils.trialDivision(i)) {
					pw.println(i);
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
